(function () { 
	
	angular
		.module('app.home')
		.controller('Homes', Homes);

	Homes.$inject = ['$location','$anchorScroll'];

	function Homes($location, $anchorScroll)
	{
		var vm = this;

		vm.scrollTo = function(id)
		{
			$location.hash(id);
			$anchorScroll();

		}
		
	}

})();