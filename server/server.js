var app = require('./app.js');

var server;
var port = 3000;

startServer(port);

module.exports = {
	start: startServer,
	stop: stopServer
};

// TODO: use actual logger

function startServer(port) {
	if (server && running) {
		console.log('Server already running on port ' + port);
		return;
	}

	server = app.listen(port);

	server.on('listening', function() {
		console.log('Server listening on ' + server.address().port);
		running = true;
	});

	server.on('close', function() {
		console.log('Exiting server');
		running = false;
	});

	server.on('error', function(err) {
		console.error('Fatal server error', err);
		running = false;
	});
}

function stopServer() {
	if (server && running) {
		server.close();
	}
}